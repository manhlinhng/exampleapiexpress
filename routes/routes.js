var routes = function (app) {

    // URI: http://localhost:3000
    app.get("/", function(req, res) {
      res.status(200).send("Welcome to our restful API");
    });

    // URI: http://localhost:3000/users
    app.get('/users', function(req, res){
        var users = [
            {
               "id":1,
               "name":"Vu Hoang",
               "age":24,
               "job":"Frontend Developer"
            },
            {
               "id":2,
               "name":"Manh Linh",
               "age":24,
               "job":"Backend Developer"
            },
            {
               "id":3,
               "name":"Anh Tricky",
               "age":23,
               "job":"Developer"
            }
         ];
        res.status(200).send(users);
    });

  }
  
  module.exports = routes;